// SPDX-License-Identifier: MIT
#include "wave.h"
#include <iostream>
#include <algorithm>
#include <cmath>

#include<Algorithm.h>

int main(int argc, char** argv)
{
	MONO_PCM pcmForWrite;
	MONO_PCM pcmForRead;

	pcmForRead.load(argv[1]);

	pcmForWrite.fs(pcmForRead.fs());
	pcmForWrite.bits(pcmForRead.bits());
	pcmForWrite.resize(pcmForRead.size(), 0);

	std::cout << "fs: " << pcmForRead.fs() << std::endl;
	std::cout << "bits: " << pcmForRead.bits() << std::endl;
	std::cout << "length: " << pcmForRead.size() << std::endl;

	size_t high_T = pcmForRead.fs() / 60; // 44100Hz / 80 Hz;
	size_t low_T = pcmForRead.fs() / 260; // 44100Hz /260 Hz;

	auto shift = high_T;
	auto range = high_T * 2;
	std::cout << "scan window range: " << range << std::endl;
	AudioProcessor::Processor proc;
	proc.pitch(1);
	proc.formant(1);
	proc.low(low_T);
	proc.high(high_T);
	for (size_t pos = 0; pos < pcmForRead.size() - range * 2; pos += shift) {
		proc.Process(pcmForRead.data() + pos, pcmForWrite.data() + pos, range);
	}

	pcmForWrite.save(argv[2]);

	return 0;
}
