// SPDX-License-Identifier: MIT
#include "wave.h"
#include <cstdio>
#include <cstdint>
#include <limits>

struct RIFF_CHUNK
{
	uint8_t chunkID[4];
	uint32_t chunkSize;
	uint8_t chunkFormType[4];
};

struct FMT_CHUNK
{
	uint8_t chunkID[4];
	uint32_t chunkSize;
	uint16_t waveFormatType;
	uint16_t formatChannel;
	uint32_t samplesPerSec;
	uint32_t bytesPerSec;
	uint16_t blockSize;
	uint16_t bitsPerSample;
};

struct DATA_CHUNK
{
	uint8_t chunkID[4];
	uint32_t chunkSize;
	uint16_t data;
};

struct WAVE_FORMAT
{
	RIFF_CHUNK riffChunk;
	FMT_CHUNK fmtChunk;
	DATA_CHUNK dataChunk;
};

template<class T>
static inline void read(FILE* fp, T& val)
{
	auto ret = fread(&val, sizeof(val), 1, fp);
	if (ret <= 0)
		perror("in fread()");
}

template<class T>
static inline size_t write(FILE * fp, const T & val) {
	return fwrite(&val, sizeof(T), 1, fp);
}

void MONO_PCM::load(const char* fileName)
{
	FILE* fp;
	WAVE_FORMAT waveFormat;
	errno_t err;

	err = fopen_s(&fp, fileName, "rb");
	if (!fp)
	{
		printf("file open error");
		exit(0);
	}

	read(fp, waveFormat.riffChunk.chunkID);
	read(fp, waveFormat.riffChunk.chunkSize);
	read(fp, waveFormat.riffChunk.chunkFormType);

	read(fp, waveFormat.fmtChunk.chunkID);
	read(fp, waveFormat.fmtChunk.chunkSize);
	read(fp, waveFormat.fmtChunk.waveFormatType);
	read(fp, waveFormat.fmtChunk.formatChannel);
	read(fp, waveFormat.fmtChunk.samplesPerSec);
	read(fp, waveFormat.fmtChunk.bytesPerSec);
	read(fp, waveFormat.fmtChunk.blockSize);
	read(fp, waveFormat.fmtChunk.bitsPerSample);

	read(fp, waveFormat.dataChunk.chunkID);
	read(fp, waveFormat.dataChunk.chunkSize);

	_fs = waveFormat.fmtChunk.samplesPerSec;
	_bits = waveFormat.fmtChunk.bitsPerSample;
	resize(waveFormat.dataChunk.chunkSize / (waveFormat.fmtChunk.bitsPerSample / 8));

	constexpr float scale = 1.0f / std::numeric_limits<int16_t>::max();
	for (auto& s : *this)
	{
		int16_t data;
		fread(&data, sizeof(data), 1, fp);
		s = static_cast<float>(data) * scale;
	}

	fclose(fp);
}

void MONO_PCM::save(const char* fileName) const
{
	WAVE_FORMAT waveFormat;

	waveFormat.riffChunk.chunkSize = 36 + size() * 2;

	// strcpy(waveFormat.fmtChunk.chunkID, "fmt ");
	waveFormat.fmtChunk.chunkSize = 16;
	waveFormat.fmtChunk.waveFormatType = 1;
	waveFormat.fmtChunk.formatChannel = 1;
	waveFormat.fmtChunk.samplesPerSec = _fs;
	waveFormat.fmtChunk.bytesPerSec = (_fs * _bits) / 8;
	waveFormat.fmtChunk.blockSize = _bits / 8;
	waveFormat.fmtChunk.bitsPerSample = _bits;

	// strcpy(waveFormat.dataChunk.chunkID, "data");
	waveFormat.dataChunk.chunkSize = size() * waveFormat.fmtChunk.blockSize;

	FILE* fp;
	errno_t err;
	err = fopen_s(&fp, fileName, "wb");
	if (!fp)
	{
		printf("file open error");
		exit(0);
	}

	fwrite("RIFF", 1, 4, fp);
	write(fp, waveFormat.riffChunk.chunkSize);
	fwrite("WAVE", 1, 4, fp);
	fwrite("fmt ", 1, 4, fp);
	write(fp, waveFormat.fmtChunk.chunkSize);
	write(fp, waveFormat.fmtChunk.waveFormatType);
	write(fp, waveFormat.fmtChunk.formatChannel);
	write(fp, waveFormat.fmtChunk.samplesPerSec);
	write(fp, waveFormat.fmtChunk.bytesPerSec);
	write(fp, waveFormat.fmtChunk.blockSize);
	write(fp, waveFormat.fmtChunk.bitsPerSample);
	fwrite("data", 1, 4, fp);
	write(fp, waveFormat.dataChunk.chunkSize);

	for (auto s : *this)
	{
		using limits = std::numeric_limits<int16_t>;
		int16_t data;
		if (s >= 1.0f) {
			data = limits::max();
		}
		else if (s <= -1.0f) {
			data = limits::min();
		}
		else {
			data = static_cast<int16_t>(s) / limits::max();
		}
		write(fp, data);
	}

	fclose(fp);
}
