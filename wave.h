// SPDX-License-Identifier: MIT
#pragma once
#include <cstdint>
#include <vector>

class MONO_PCM : private std::vector<float>
{
	uint32_t _fs;
	uint16_t _bits;
public:
	MONO_PCM() :_fs(0), _bits(0) {}
	using std::vector<float>::size;
	using std::vector<float>::resize;
	using std::vector<float>::data;

	uint32_t fs()const { return _fs; }
	uint32_t fs(uint32_t value) { return _fs = value; }
	uint16_t bits() const { return _bits; }
	uint16_t bits(uint16_t value) { return _bits = value; }

	void load(const char* fileName);
	void save(const char* fileName)const;
};
